
# libpath for local system
.libPaths("/Users/batool/Documents/PhD_GlobeWQ/RPackages")


##Import libraries
library(readxl)
library(xlsx)
library(raster)
library(tidyverse)
library(zoo)
library(RColorBrewer)
library(data.table)
library(rgdal)

rm(list = ls())
gc()

######################################################################################
# Estimate for EU 27 and remove UK from the estimates as UK is not part of EU27

# Fertilizer
method_list <- c(1:2)

method = "1"
function_df <- function(method) {
  
  # change name of the colomn for method type
  cols = c("Nfert_total_Tg")
  f_name <- paste0("Inputs/Fertilizer/EU_27_method_", method, "_Nfert_total_tonne_1850_2019.csv")
  
  df <- read.csv(paste0("Inputs/Fertilizer/method_", method, "_Nfert_total_tonne_1850_2019.csv")) %>%
    filter(Country == "Denmark"    |Country == "France"        | Country == "Germany" 
           |Country == "Italy"     |Country == "Netherlands"   | Country == "Hungary"
           | Country == "Poland"   | Country == "Spain"        | Country == "Sweden" 
           |Country == "Belgium"   |Country == "Bulgaria"      |Country == "Ireland"
           |Country == "Greece"    |Country == "Croatia"       |Country == "Austria"
           |Country == "Slovenia"  |Country == "Slovakia"      |Country == "Luxembourg"
           | Country == "Latvia"   | Country == "Finland"      | Country == "Malta"
           | Country == "Lithuania"   | Country == "Romania"   | Country == "Czechia"  
           | Country == "Estonia"     | Country == "Cyprus"    | Country == "Portugal") %>%
    group_by(Year)%>%
    mutate(Nfert_total_EU27 = sum(Nfert_total_tonne, na.rm= T)) %>%
    filter(Country == "Austria") %>%
    mutate(Country = replace(Country, Country == "Austria", "EU27"))%>%
    dplyr::select(-Study_ID, -X, -Nfert_total_tonne)%>%
    mutate(Nfert_total_Tg = Nfert_total_EU27/10^6) %>%
    dplyr::select(Country, Year, Nfert_total_Tg)%>%
    rename_at(., cols, list(~paste0(., "_", method)))
  
  # function to round values to 3 digits
  round_df <- function(df, digits) {
    nums <- vapply(df, is.numeric, FUN.VALUE = logical(1))
    
    df[,nums] <- round(df[,nums], digits = digits)
    
    (df)
  }
  df_1 <- round_df(df, digits=3)
  
  # write csv file
  write.csv(df_1 ,  f_name, row.names=FALSE)
  
}
lapply(method_list, function_df)

# Manure
method_list <- c(1:4)
function_df <- function(method) {
  
  # change name of the colomn for method type
  cols = c("Man_total_Tg")
  f_name <- paste0("Inputs/Manure/EU_27_method_", method, "_Man_total_tonne_1850_2019.csv")
  
  df <- read.csv(paste0("Inputs/Manure/method_", method, "_Man_total_tonne_1850_2019.csv")) %>%
    filter(Country == "Denmark"    |Country == "France"        | Country == "Germany" 
           |Country == "Italy"     |Country == "Netherlands"   | Country == "Hungary"
           | Country == "Poland"   | Country == "Spain"        | Country == "Sweden" 
           |Country == "Belgium"   |Country == "Bulgaria"      |Country == "Ireland"
           |Country == "Greece"    |Country == "Croatia"       |Country == "Austria"
           |Country == "Slovenia"  |Country == "Slovakia"      |Country == "Luxembourg"
           | Country == "Latvia"   | Country == "Finland"      | Country == "Malta"
           | Country == "Lithuania"   | Country == "Romania"   | Country == "Czechia"  
           | Country == "Estonia"     | Country == "Cyprus"    | Country == "Portugal") %>%
    group_by(Year)%>%
    mutate(Man_total_EU27 = sum(Man_total_tonne, na.rm= T)) %>%
    filter(Country == "Austria") %>%
    mutate(Country = replace(Country, Country == "Austria", "EU27"))%>%
    dplyr::select(-Study_ID, -X, -Man_total_tonne)%>%
    mutate(Man_total_Tg = Man_total_EU27/10^6) %>%
    dplyr::select(Country, Year, Man_total_Tg)%>%
    rename_at(., cols, list(~paste0(., "_", method)))
  
  # function to round values to 3 digits
  round_df <- function(df, digits) {
    nums <- vapply(df, is.numeric, FUN.VALUE = logical(1))
    
    df[,nums] <- round(df[,nums], digits = digits)
    
    (df)
  }
  df_1 <- round_df(df, digits=3)
  
  # write csv file
  write.csv(df_1 ,  f_name, row.names=FALSE)
  
}
lapply(method_list, function_df)

# N_removal
method_list <- c(1:2)
function_df <- function(method) {
  
  # change name of the colomn for method type
  cols = c("N_out_total_Tg")
  f_name <- paste0("Inputs/N_removal/EU_27_method_", method, "_N_out_total_tonne_1850_2019.csv")
  
  df <- read.csv(paste0("Inputs/N_removal/method_", method, "_N_out_total_tonne_1850_2019.csv")) %>%
    filter(Country == "Denmark"    |Country == "France"        | Country == "Germany" 
           |Country == "Italy"     |Country == "Netherlands"   | Country == "Hungary"
           | Country == "Poland"   | Country == "Spain"        | Country == "Sweden" 
           |Country == "Belgium"   |Country == "Bulgaria"      |Country == "Ireland"
           |Country == "Greece"    |Country == "Croatia"       |Country == "Austria"
           |Country == "Slovenia"  |Country == "Slovakia"      |Country == "Luxembourg"
           | Country == "Latvia"   | Country == "Finland"      | Country == "Malta"
           | Country == "Lithuania"   | Country == "Romania"   | Country == "Czechia"  
           | Country == "Estonia"     | Country == "Cyprus"    | Country == "Portugal")%>%
    group_by(Year)%>%
    mutate(N_out_total_EU27 = sum(N_out_total_tonne, na.rm= T)) %>%
    filter(Country == "Austria") %>%
    mutate(Country = replace(Country, Country == "Austria", "EU27"))%>%
    dplyr::select(-Study_ID, -X, -N_out_total_tonne)%>%
    mutate(N_out_total_Tg = N_out_total_EU27/10^6) %>%
    dplyr::select(Country, Year, N_out_total_Tg)%>%
    rename_at(., cols, list(~paste0(., "_", method)))
  
  # function to round values to 3 digits
  round_df <- function(df, digits) {
    nums <- vapply(df, is.numeric, FUN.VALUE = logical(1))
    
    df[,nums] <- round(df[,nums], digits = digits)
    
    (df)
  }
  df_1 <- round_df(df, digits=3)
  
  # write csv file
  write.csv(df_1 ,  f_name, row.names=FALSE)
  
}
lapply(method_list, function_df)

# Fixation
path = "Inputs/Fixation/"
function_df <- function(path) {
  # change name of the colomn for method type
  f_name <- paste0(path, "EU_27_BNF_total_tonne_1850_2019.csv")
  df <- read.csv(paste0(path, "BNF_total_tonne_1850_2019.csv")) %>%
    filter(Country == "Denmark"    |Country == "France"        | Country == "Germany" 
           |Country == "Italy"     |Country == "Netherlands"   | Country == "Hungary"
           | Country == "Poland"   | Country == "Spain"        | Country == "Sweden" 
           |Country == "Belgium"   |Country == "Bulgaria"      |Country == "Ireland"
           |Country == "Greece"    |Country == "Croatia"       |Country == "Austria"
           |Country == "Slovenia"  |Country == "Slovakia"      |Country == "Luxembourg"
           | Country == "Latvia"   | Country == "Finland"      | Country == "Malta"
           | Country == "Lithuania"   | Country == "Romania"   | Country == "Czechia"  
           | Country == "Estonia"     | Country == "Cyprus"    | Country == "Portugal") %>%
    group_by(Year)%>%
    mutate(BNF_total_EU27 = sum(BNF_total_tonne, na.rm= T)) %>%
    filter(Country == "Austria") %>%
    mutate(Country = replace(Country, Country == "Austria", "EU27"))%>%
    dplyr::select(-Study_ID, -X, -BNF_total_tonne)%>%
    mutate(BNF_total_Tg = BNF_total_EU27/10^6) %>%
    dplyr::select(Country, Year, BNF_total_Tg)
  
  # function to round values to 3 digits
  round_df <- function(df, digits) {
    nums <- vapply(df, is.numeric, FUN.VALUE = logical(1))
    
    df[,nums] <- round(df[,nums], digits = digits)
    
    (df)
  }
  df_1 <- round_df(df, digits=3)
  
  # write csv file
  write.csv(df_1 ,  f_name, row.names=FALSE)
  
}
function_df(path)

# Deposition
path = "Inputs/Deposition/"
function_df <- function(path) {
  # change name of the colomn for method type
  f_name <- paste0(path, "EU_27_N_dep_total_tonne_1850_2019.csv")
  df <- read.csv(paste0(path, "N_dep_total_tonne_1850_2019.csv")) %>%
    filter(Country == "Denmark"    |Country == "France"        | Country == "Germany" 
           |Country == "Italy"     |Country == "Netherlands"   | Country == "Hungary"
           | Country == "Poland"   | Country == "Spain"        | Country == "Sweden" 
           |Country == "Belgium"   |Country == "Bulgaria"      |Country == "Ireland"
           |Country == "Greece"    |Country == "Croatia"       |Country == "Austria"
           |Country == "Slovenia"  |Country == "Slovakia"      |Country == "Luxembourg"
           | Country == "Latvia"   | Country == "Finland"      | Country == "Malta"
           | Country == "Lithuania"   | Country == "Romania"   | Country == "Czechia"  
           | Country == "Estonia"     | Country == "Cyprus"    | Country == "Portugal") %>%
    group_by(Year)%>%
    mutate(N_dep_total_EU27 = sum(N_dep_total_tonne, na.rm= T)) %>%
    filter(Country == "Austria") %>%
    mutate(Country = replace(Country, Country == "Austria", "EU27"))%>%
    dplyr::select(-Study_ID, -X, -N_dep_total_tonne)%>%
    mutate(N_dep_total_Tg = N_dep_total_EU27/10^6) %>%
    dplyr::select(Country, Year, N_dep_total_Tg)
  
  # function to round values to 3 digits
  round_df <- function(df, digits) {
    nums <- vapply(df, is.numeric, FUN.VALUE = logical(1))
    
    df[,nums] <- round(df[,nums], digits = digits)
    
    (df)
  }
  df_1 <- round_df(df, digits=3)
  
  # write csv file
  write.csv(df_1 ,  f_name, row.names=FALSE)
  
}
function_df(path)

# 16 N surplus estimates 

N_sur <- read.csv("Inputs/N_surplus/EU27_16_N_sur_total_Tg_1850_2019.csv")

## Combine all components together

# N inputs 
Nfert_1 <- read.csv("Inputs/Fertilizer/EU_27_method_1_Nfert_total_tonne_1850_2019.csv")
Nfert_2 <- read.csv("Inputs/Fertilizer/EU_27_method_2_Nfert_total_tonne_1850_2019.csv")
Man_1   <- read.csv("Inputs/Manure/EU_27_method_1_Man_total_tonne_1850_2019.csv")
Man_2   <- read.csv("Inputs/Manure/EU_27_method_2_Man_total_tonne_1850_2019.csv")
Man_3   <- read.csv("Inputs/Manure/EU_27_method_3_Man_total_tonne_1850_2019.csv")
Man_4   <- read.csv("Inputs/Manure/EU_27_method_4_Man_total_tonne_1850_2019.csv")
BNF     <- read.csv("Inputs/Fixation/EU_27_BNF_total_tonne_1850_2019.csv")
N_dep   <- read.csv("Inputs/Deposition/EU_27_N_dep_total_tonne_1850_2019.csv")

# N removal
N_out_1 <- read.csv("Inputs/N_removal/EU_27_method_1_N_out_total_tonne_1850_2019.csv")
N_out_2 <- read.csv("Inputs/N_removal/EU_27_method_2_N_out_total_tonne_1850_2019.csv")

df <- full_join(Nfert_1, Nfert_2, by = c("Country", "Year")) %>%
  full_join(., Man_1, by = c("Country", "Year")) %>%
  full_join(., Man_2, by = c("Country", "Year")) %>%
  full_join(., Man_3, by = c("Country", "Year")) %>%
  full_join(., Man_4, by = c("Country", "Year")) %>%
  full_join(., BNF, by = c("Country", "Year")) %>%
  full_join(., N_dep, by = c("Country", "Year")) %>%
  full_join(., N_out_1, by = c("Country", "Year")) %>%
  full_join(., N_out_2, by = c("Country", "Year")) %>%
  full_join(., N_sur, by = c("Country", "Year")) %>%
  group_by(Year) %>%
  mutate(N_sur_mean_Tg = mean(c(N_sur_total_Tg_1,N_sur_total_Tg_2,N_sur_total_Tg_3,N_sur_total_Tg_4,
                                N_sur_total_Tg_5,N_sur_total_Tg_6,N_sur_total_Tg_7,N_sur_total_Tg_8,
                                N_sur_total_Tg_9,N_sur_total_Tg_10,N_sur_total_Tg_11,N_sur_total_Tg_12,
                                N_sur_total_Tg_13,N_sur_total_Tg_14,N_sur_total_Tg_15,N_sur_total_Tg_16)),
         N_sur_min_Tg = min(c(N_sur_total_Tg_1,N_sur_total_Tg_2,N_sur_total_Tg_3,N_sur_total_Tg_4,
                              N_sur_total_Tg_5,N_sur_total_Tg_6,N_sur_total_Tg_7,N_sur_total_Tg_8,
                              N_sur_total_Tg_9,N_sur_total_Tg_10,N_sur_total_Tg_11,N_sur_total_Tg_12,
                              N_sur_total_Tg_13,N_sur_total_Tg_14,N_sur_total_Tg_15,N_sur_total_Tg_16)),
         N_sur_max_Tg = max(c(N_sur_total_Tg_1,N_sur_total_Tg_2,N_sur_total_Tg_3,N_sur_total_Tg_4,
                              N_sur_total_Tg_5,N_sur_total_Tg_6,N_sur_total_Tg_7,N_sur_total_Tg_8,
                              N_sur_total_Tg_9,N_sur_total_Tg_10,N_sur_total_Tg_11,N_sur_total_Tg_12,
                              N_sur_total_Tg_13,N_sur_total_Tg_14,N_sur_total_Tg_15,N_sur_total_Tg_16)),
         N_sur_sd_Tg = sd(c(N_sur_total_Tg_1,N_sur_total_Tg_2,N_sur_total_Tg_3,N_sur_total_Tg_4,
                            N_sur_total_Tg_5,N_sur_total_Tg_6,N_sur_total_Tg_7,N_sur_total_Tg_8,
                            N_sur_total_Tg_9,N_sur_total_Tg_10,N_sur_total_Tg_11,N_sur_total_Tg_12,
                            N_sur_total_Tg_13,N_sur_total_Tg_14,N_sur_total_Tg_15,N_sur_total_Tg_16)),
         Nfert_mean_Tg = mean(c(Nfert_total_Tg_1, Nfert_total_Tg_2)),
         Nfert_sd_Tg = sd(c(Nfert_total_Tg_1, Nfert_total_Tg_2)),
         Man_mean_Tg = mean(c(Man_total_Tg_1, Man_total_Tg_2,Man_total_Tg_3,Man_total_Tg_4)),
         Man_sd_Tg = sd(c(Man_total_Tg_1, Man_total_Tg_2,Man_total_Tg_3,Man_total_Tg_4)),
         N_out_mean_Tg = mean(c(N_out_total_Tg_1, N_out_total_Tg_2)),
         N_out_sd_Tg = sd(c(N_out_total_Tg_1, N_out_total_Tg_2)),
         N_inp_Tg = sum(Nfert_total_Tg_1, Man_total_Tg_1, BNF_total_Tg, N_dep_total_Tg)) %>%
  dplyr::select(Country, Year, Nfert_total_Tg_1, Nfert_total_Tg_2,Nfert_mean_Tg, Nfert_sd_Tg, Man_total_Tg_1, Man_total_Tg_2,Man_total_Tg_3,Man_total_Tg_4, Man_mean_Tg,Man_sd_Tg,BNF_total_Tg, N_dep_total_Tg,
                N_out_total_Tg_1, N_out_total_Tg_2, N_out_mean_Tg, N_out_sd_Tg, N_sur_mean_Tg,N_sur_sd_Tg,
                N_sur_total_Tg_1,N_sur_total_Tg_2,N_sur_total_Tg_3,N_sur_total_Tg_4,
                N_sur_total_Tg_5,N_sur_total_Tg_6,N_sur_total_Tg_7,N_sur_total_Tg_8,
                N_sur_total_Tg_9,N_sur_total_Tg_10,N_sur_total_Tg_11,N_sur_total_Tg_12,
                N_sur_total_Tg_13,N_sur_total_Tg_14,N_sur_total_Tg_15,N_sur_total_Tg_16,N_inp_Tg, Area_ha_EU27)


#rm(N_sur, Nfert_1, Nfert_2, Man_1, Man_2, Man_3, Man_4, BNF, N_dep, N_out_1, N_out_2)
# function to round values to 3 digits
round_df <- function(df, digits) {
  nums <- vapply(df, is.numeric, FUN.VALUE = logical(1))
  
  df[,nums] <- round(df[,nums], digits = digits)
  
  (df)
}

df_1 <- round_df(df, digits=4)



### Estimates of N input's contributions and NUE for two phases for EU27

# 1 = Early development phase (1940-1985) # 46 years
# 3 = Sustainable intensification phase  (1985-2019) # 34 years

## Early development phase (1940-1985)
df_1940_1985 <- df_1 %>% 
  filter(Year > 1939 & Year < 1986)

# Sustainable intensification phase  (1985-2019)
df_1986_2019 <- df_1 %>% 
  filter(Year > 1985 & Year < 2020)


## Cumulative sum
sum_1940_1985 <- colSums(df_1940_1985[,-(1:2)]) %>% as.data.frame(.) %>% filter(. != max(.))
colnames(sum_1940_1985 ) <- c("1940_1985") # Early development phase
sum_1986_2019 <- colSums(df_1986_2019[,-(1:2)]) %>% as.data.frame(.)%>% filter(. != max(.))
colnames(sum_1986_2019) <- c("1986_2019") #Sustainable intensification phase

df_final <- cbind(sum_1940_1985,sum_1986_2019 )
df_final_Pg <- df_final[, 1:ncol(df_final)]/1000 # convert values to petagram
df_final_Pg <- round_df(df_final_Pg, digits=4)


# Phase 1 : Early development phase = 1940-1985 (46 years)

# N input (Nfert+Man+BNF+Dep) (Unit  = TgN)

# 0.28+0.33+0.22+0.14 = 0.97 (1920_1985) 46 Years
# 0.36+0.26+0.13+0.15 = 0.90 (1986-2019) 34 years


# 1940-1985
# 0.28+0.33+0.22+0.14 = 0.97
# contribution by individual component of N inputs
#> 0.28/0.97 * 100= 28.86598
#> 0.33/0.97 * 100 = 34.02062
# 0.22/0.97 * 100 = 22.68041
#> 0.14/0.97 * 100= 14.43299
#> 28.86598 + 34.02062+22.68041+14.43299 =  100
#> 
#> 
#> # Convert values in PgN/yr for early development phase (1920_1985)
# For that we multiply the value with 1000 and divide by 46
# fertilizer = (0.28 * 1000)/46=  6.086957 PgN/yr
# manure  =  (0.33 * 1000)/46 = 7.173913
# fixation =  (0.22 * 1000)/46 =  4.782609
# deposition =  (0.14 * 1000)/46 =  3.043478

# total N inputs = 6.086957 + 7.173913+4.782609+3.043478 =  21.08696
# N surplus = 21.08696 - 10 = 11 TgN/yr
#> 
#> 
#> # 1986-2019
# 0.36+0.26+0.13+0.15 = 0.90
# contribution by individual component of N inputs
#> 0.36/0.90 * 100= 40
#> 0.26/0.90 * 100 = 28.88889
# 0.13/0.90 * 100 =  14.44444
#> 0.15/0.90 * 100= 16.66667
#> 40 + 28.88889+ 14.44444+16.66667 =  100
#> 
#> 
#> Proportion of N inputs not utilized during early development phase = (11 / 21.08696) * 100 ≈ 52.2%
#> 
#> 
#>  # Convert values in PgN/yr for early development phase ( 1986-2019)
#>  
#>  # For that we multiply the value with 1000 and divide by 46
# fertilizer = (0.36 * 1000)/34 = 10.58824 PgN/yr
# manure  =  (0.26 * 1000)/34=  7.647059
# fixation =  (0.13 * 1000)/34=  3.823529
# deposition = (0.15 * 1000)/34=  4.411765

# total N inputs = 10.58824 + 7.647059+3.823529+4.411765 = 26.47059
# total N output  = (0.42 * 1000)/34 = 12.35294
# N surplus = 26.47059 - 12 = 14.47059 TgN/yr
#>  
#>  #> Proportion of N inputs not utilized during intensification = (14.47059/ 26.47059 ) * 100 ≈ 54.66667%
#
#N_out_total_Tg (mean of 2 N outputs)
#0.46
#0.42

#N_sur_mean_Tg
#0.97 - 0.46 = 0.51
#0.90 - 0.42 = 0.48

## Calculate fractions

#1940_1985

Nfert = 0.28
Man =  0.33
BNF =  0.22
Dep = 0.14

Ninp = Nfert + Man + BNF + Dep  # 1.18 Pg
Nfert_fr = (Nfert/Ninp) * 100 # 28.86598  %
Man_fr = (Man/Ninp) * 100     # 34.02062 %
BNF_fr = (BNF/Ninp) * 100       # 22.68041 %
Dep_fr = (Dep/Ninp) * 100       # 14.43299 %


#1986_2019
Nfert = 0.36
Man = 0.26
BNF =  0.13
Dep = 0.15

Ninp = Nfert + Man + BNF + Dep # 0.90 Pg
Nfert_fr = (Nfert/Ninp) * 100 # 40 %
Man_fr = (Man/Ninp) * 100     # 28.88889%
BNF_fr = (BNF/Ninp) * 100       # 14.44444 %
Dep_fr = (Dep/Ninp) * 100       # 16.66667 %


write.csv(df_1, "Inputs/Nsur_individual_components/EU_27_Nsur_components_1850_2019.csv")


## convert values in PgN into TgN/yr 

# N inputs
#1940_1985
# 0.28+0.33+0.22+0.14 = 0.97 (Fertilizer, Manure, fixation, deposition)
#> (0.28 * 1000)/46 =  6.086957
# (0.33 * 1000)/46 =  7.173913
# > (0.22 * 1000)/46= 4.782609
# (0.14 * 1000)/46 = 3.043478

# 6.086957+7.173913+4.782609+3.043478 = 21.08696
# fertilizer = (6.086957/21.08696) * 100= 28.86598 %
# manure = (7.173913/21.08696) * 100= 34.02061 %
# fixation =  (4.782609/21.08696) * 100= 22.68041 %
# deposition =  (3.043478/21.08696) * 100= 14.43299 %

# standard deviation
# fertilizer = 0
# manure = 0.07 Pg = (0.07 * 1000)/46 =  1.521739

#> For N output
#> > (0.46 * 1000)/46=  10
#> # standard deviation
# N output = 0.03 Pg = (0.03 * 1000)/46 =  0.6521739
#> For N surplus
#> > (0.51 * 1000)/46 =  11.08696
# # standard deviation
# 0.05 Tg = (0.05 * 1000)/46 =  1.086957

# 1986-2019 for N inputs
# 0.36+0.26+0.13+0.15 = 0.90 
#> (0.36 * 1000)/36 = 10
#> (0.26 * 1000)/36 = 7.222222
#> > (0.13 * 1000)/36= 3.611111
#> > (0.15 * 1000)/36= 4.166667
#> 
#> # standard deviations
#> # fertilizer = 0
# manure = 0.01 Pg = (0.01 * 1000)/36 =  0.2777778
#> 
#> For N output
#> > (0.42 * 1000)/36=  11.66667
#> # SD, 0.01 Pg = (0.01 * 1000)/36 =  0.2777778
#> For N surplus
#> > (0.48 * 1000)/36 =  13.33333
#> #> # SD, 0.02 Pg = (0.02 * 1000)/36 =  0.5555556
###########################################################################